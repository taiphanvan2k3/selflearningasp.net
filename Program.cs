using ASPNetCore01.Services;
using Microsoft.AspNetCore.Mvc.Razor;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Đăng kí vào ứng dụng loạt các dịch vụ để hoạt động đến 
builder.Services.AddControllersWithViews();

// Đăng kí các dịch vụ liên quan đến các trang Razoz
builder.Services.AddRazorPages();
builder.Services.Configure<RazorViewEngineOptions>(options =>
{
    // Mặc định là nó sẽ tìm các file template ở thư mục Views
    // Nhưng bây giờ ta sẽ thiết lập thêm, nếu không tìm thấy ở thư mục Views
    // thì tìm tiếp ở thư mục MyView chẳng hạn

    // Mặc định nó sẽ tìm các file template ở Views/Controller/<tên Action>.cshtml
    // Giờ thiết lập thêm nếu không tìm thấy ở Views thì tìm tiếp ở: MyView/Controller/<tên Action>.cshtml

    // {0} -> tên Action
    // {1} -> Tên controller
    // {2} -> nếu có thì là tên Area
    options.ViewLocationFormats.Add("/MyView/{1}/{0}" + RazorViewEngine.ViewExtension);
});

// Đăng kí dịch vụ
// Singleton: chỉ tạo ra 1 đối tượng dịch vụ
// Transient: thì mỗi lần truy vấn để lấy ra dịch vụ thì 1 đối tượng mới được tạo ra
// Scoped: mỗi phiên truy cập nếu có lấy dịch vụ này ra thì 1 đối tượng mới được tạo ra 

// C1: Tên dịch vụ là ProductService
// builder.Services.AddSingleton<ProductService>();

// C2: Tên dịch vụ là ProductService và đối tượng triển khai là ProductService
// builder.Services.AddSingleton<ProductService, ProductService>();

// C3:Tạo ra 1 dịch vụ có tên là typeof(ProductService) và khi dịch vụ này được tạo ra thì
// nó tạo ra đối tượng ProductService
// builder.Services.AddSingleton(typeof(ProductService));

// C4:
builder.Services.AddSingleton(typeof(ProductService),typeof(ProductService));

// SỬ dụng c2,c4 thì khi lấy ra đối tượng ProductService thì có thể tạo ra các
// đối tượng ProductService hoặc là các đối tượng kế thừa, implement từ ProductService
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
// Truy cập các file tĩnh lưu trong thư mục wwwroot
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

// Tạo ra các điểm endpoints bằng việc tạo ra một route tên là defaul
// Nó phân tích url nếu có :/{tên controller}/{action}/{id?} thì nó sẽ tạo ra
// Controller theo tên, sau đó chuyển request cho controller và thi hình phương thức Action
// Nếu truy cập url có địa chỉ ABC/xyz thì nó sẽ tìm controller có tên là Abc và gọi phương
// thức xyz trong controller đó 
// Ví dụ truy cập Home/Index thì truy cập đến Controller có tên là Home và thi hành phương
// thức Index
// Ngoài ra ở phần MapControllerRouter:
//    + Nếu không có action thì mặc định action là Index
//    + Nếu không thiết lập Controller thì mặc định Controller là Home
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

// Tạo ra các điểm endpoint đến các trang Razoz page
// Kich hoạt điều này đến ứng dụng vừa có thể truy cập đến các trang Razoz
// vừa có thể truy cập đến các Controller trong mô hình MVC
app.MapRazorPages();
app.Run();
