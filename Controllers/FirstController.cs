// Phải có controller này thì mới có thể truy cập đến được First.xyz
using Microsoft.AspNetCore.Mvc;
using ASPNetCore01.Services;
namespace ASPNetCore01.Controllers
{
    // Để lớp này được coi là Controller thì nó phải kế thừa từ Controller
    public class FirstController : Controller
    {
        private readonly ILogger<FirstController> _logger;
        private readonly IWebHostEnvironment _env;

        private readonly ProductService _productService;

        // Nếu tại Controller/View nào muốn sử dụng dịch vụ này thì ta cần inject nó vào tại hàm khởi tạo
        public FirstController(ILogger<FirstController> logger, IWebHostEnvironment env, ProductService productService)
        {
            // Ta đã add service tại Program.cs nên giờ có thể dùng ProductService
            _logger = logger;
            _env = env;
            _productService = productService;
        }

        public string Index()
        {
            // _logger.LogInformation("Show Index Action");
            // Console.WriteLine("Show Index action");

            // Thay vì dùng _logger.Log(LogLevel.Warning,"Thông báo warning");
            // _logger.Log(LogLevel.Warning,"Thông báo warning");

            // thì có thể dùng:
            _logger.LogInformation("First/Index");
            // _logger.LogWarning("Test information");
            // _logger.LogTrace("Test information");
            // _logger.LogDebug("Test information");
            // _logger.LogError("Test information");
            // _logger.LogCritical("Test information");
            return "Tôi là Index,xin chào Tài :)))))).";
        }

        public string ShowHello() => "Hello Tài";


        // Action là 1 phương thức public, không được khai báo là static,
        // có thể trả về bất kì đối tượng hay kiểu dữ liệu nào.
        // Nếu có dữ liệu trả về thì nó sẽ được convert thành string và thiết lập
        //nội dung trả về cho client
        public object ShowDateTime()
        {
            // Lúc này nó sẽ trả về DateTime rồi convert qua string
            return DateTime.Now;
            // return @$"Hôm nay là {DateTime.Now.Day}/{DateTime.Now.Month}/{DateTime.Now.Year}";
        }

        public int[] ShowArray() => new int[] { 1, 2, 3, 4, 5, 6, 7 };

        public IActionResult Readme()
        {
            var content = @"Xin chào các bạn,Hôm nay là ngày 02/06/2023";
            // Khi gọi phuơng thức Content nó sẽ trả về một đối tượng ContentResult
            return Content(content, "plain/text");
        }

        public IActionResult GetMyImage()
        {
            _logger.LogInformation("Thư mục hiện hành là:" + _env.ContentRootPath);
            // Với thư mục controller thì nó tính từ thư mục gốc là thư mục chứa project
            var bytes = System.IO.File.ReadAllBytes("./Files/avatar.jpg");
            return File(bytes, "image/png");
        }

        public IActionResult IphonePrice()
        {
            return Json(
                new
                {
                    ProductName = "Iphone",
                    Price = "100$"
                }
            );
        }


        public IActionResult Privacy()
        {
            // Chuyển đến 1 url của trang web của mình
            var url = Url.Action("Privacy", "Home");
            _logger.LogInformation("Chuyển hướng đến Home privacy");
            if (url == null)
                url = "";
            return LocalRedirect(url);
        }

        public IActionResult RedirectAnyUrl()
        {
            // Có thể redirect đến bất kì url nào miễn là url đó hợp lệ nhớ vào phương thức Redirect
            return Redirect("https://www.youtube.com/watch?v=-MctlvnYAC4&list=PLwJr0JSP7i8BERdErX9Ird67xTflZkxb-&index=68");
        }

        public IActionResult HelloView_1()
        {
            // Yêu cầu sử dụng Razoz engine đọc và thi hành file .cshtml (template)

            // Th1: mở file này,thi hành và trả kết quả về cho client
            // Dạng 1: View(templat) trong đó template là đường dẫn đến .cshtml
            return View("./MyView/xinchao.cshtml");
        }

        public IActionResult HelloView_2(string username)
        {
            if (string.IsNullOrEmpty(username))
                username = "Khách";
            // Yêu cầu sử dụng Razoz engine đọc và thi hành file .cshtml (template)

            // Th1: mở file này,thi hành và trả kết quả về cho client
            // Dạng 2: View(template,model)
            // Tham số username có thể lấy từ form hoặc thay đổi từ url sau: /First/HelloView_2?username=Taiphanvan
            return View("./MyView/xinchao.cshtml", username);
        }

        public IActionResult HelloViewOfController(string username)
        {
            if (string.IsNullOrEmpty(username))
                username = "Khách VIP";
            // Mặc Razoz engine truy cập đến file xinchao.cshtml trong /Views/First/xinchao.cshtml
            // Nhưng nếu có thiết lập thêm tìm kiếm ở 1 thư mục nào đó khác tại Program.cs thì sẽ tiếp tục tìm tiếp
            // Vd nếu không tìm thấy ở /Views/First/xinchao.cshtml thì sẽ đi tìm đến template ở /MyView/First/xinchao.cshtml
            return View("xinchao", username);
        }

        public IActionResult HelloViewOfController_2(string username)
        {
            if (string.IsNullOrEmpty(username))
                username = "Khách VIP" + " " + _env.ContentRootPath;
            // Nếu không thiết lập template thì nó tự động gọi đến file cshtml trong
            // thư mục First trong Views: /Views/First/HelloViewOfController_2.cshtml

            // Nếu muốn truyền model qua trường hợp này thì model không thể là string
            // vì nó hiểu nhầm đây là template chứ không phải model. Do đó ta phải truyền object qua
            // để nói rằng đây là model chứ không phải template dẫn đến file cshtml

            return View((object)username);
        }

        [TempData]
        public string StatusMessage { get; set; }
        public IActionResult ViewProduct(int? id)
        {
            // Do trong phần thiết lập root ở Program có thiết lập 1 tham số là ID
            // Do đó nếu action của ta có tham số là id thì giá trị id có thể được
            // binding đến bởi url do đó ta chỉ cần /2 thì giá trị id = 2 rồi

            // Nhập vào url: /First/ViewProduct/2
            // Hoặc /First/ViewProduct?idx=2. Nên dùng cách này trong trong hợp tham số
            // không phải tên là id mà vd idx chẳng hạn thì cần ?idx=2
            var product = _productService.Where(p => p.Id == id).FirstOrDefault();
            if (product == null)
            {
                // C1: khi reload lại trang thì TempData sẽ bị xoá.
                // TempData["StatusMessage"] = "Không tìm thấy sản phẩm";
                StatusMessage = "Không tìm thấy sản phẩm này.";
                return RedirectToAction("Index", "Home");
            }
            _logger.LogInformation($"Id san pham la {id}");
            // Mặc định sẽ gọi đến template cshtml trong /View/First/ViewProduct.cshtml
            // Nếu không tìm thấy thì gọi đến /MyView/First/ViewProduct.cshtml
            // C1:
            // return View(product);

            // C2: ViewData
            // ViewData["title"] = product.Name;
            // ViewData["product"] = product;
            // return View("ViewProduct2");
            // C3: ViewBag
            ViewBag.product = product;
            return View("ViewProduct3");
        }
    }
}