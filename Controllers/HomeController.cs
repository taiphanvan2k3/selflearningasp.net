﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using ASPNetCore01.Models;

namespace ASPNetCore01.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    // Chỉ cần có phương thức là đủ để
    public string HiHome() => "Xin chào các bạn, Tôi là Tài";

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
