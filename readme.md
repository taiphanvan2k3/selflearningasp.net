# Buổi 1
## Controller
- Là một lớp kế thừa lớp Controler
- Action trong controller là một phương thức public (không được static)
- Action trả về bất kì dữ liệu nào, thường là IActionResult
## View
- Là file .cshtml
- View cho Action lưu trữ lại: /View/ControllerName/ActionName.cshtml
- Cũng có thể configure để có thể tìm kiếm file cshtml ở 1 thư mục nào
đó vd MyView
-> Cách thiết lập:
options.ViewLocationFormats.Add("/MyView/{1}/{0} + RazorViewEngine.ViewExtension);
## Truyền dữ liệu sang View
- Dùng Model
- Dùng ViewBag vd ViewBag.Product = product;
- Dùng ViewData (hoạt động theo kiểu Dictionary)
vd: ViewData["Shop"] = shop;
## TempData: 